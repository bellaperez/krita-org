---
title: "Krita 5.1.4 正式版已经推出"
date: "2022-12-15"
categories: 
  - "news-zh"
  - "officialrelease-zh"
---

今天我们为大家带来了 Krita 5.1.4 版，修复了一些程序缺陷。因为我们在这之后会更新 Krita 的依赖程序库，因此此版大概率将是 5.1 系列的最后一个版本。即将发布的 5.2 版将带来许多新特性，敬请期待！

## 已修复的程序缺陷：

- 矢量形状不能对调当前的前景色/背景色 ([BUG:461692](https://bugs.kde.org/show_bug.cgi?id=461692))
- 修复“粘贴为活动图层”时的崩溃 ([BUG:462223](https://bugs.kde.org/show_bug.cgi?id=462223))
- 图层样式：正确标识外发光样式，而不是将它错误标识为内发光 ([BUG:462091](https://bugs.kde.org/show_bug.cgi?id=462091))
- 解析 ICC 特性文件的色彩转换特性。感谢 Rasyuga 提供补丁 (BUG:45911)
- 修复 ICC 特性文件的原色和白点检测。感谢 Rasyuga 提供补丁
- 移除在键盘快捷键配置页面中的两个已废弃的操作
- 修复在使用非整数倍显示缩放时的一些显示错误问题 (BUG:441216, 460577, 461912)
- 修复像素笔刷引擎之外的笔刷引擎的四方连续显示 (BUG:460299)
- 修复了测量工具和渐变工具在深色背景下的可读性
- 修复在变形操作应用太快时会造成数据丢失 (BUG:460557, 461109)
- 安卓版本：禁用资源位置的更改操作
- 安卓版本：禁用触摸屏辅助按钮面板 (因为它的某些按钮已经彻底失灵，而且我们也在重写 Krita 的触摸功能) BUG:461634
- 安卓版本：禁用新建窗口 (安卓系统没有窗口管理)
- 安卓版本：禁用那些会创建多个窗口的工作区 (安卓系统没有窗口管理)
- 安卓版本：启用 TIFF 导入和导出
- 安卓版本：移除分离画布窗口 (安卓系统没有窗口管理)
- TIFF：修复不透明通道不统一和 Photoshop 型多图层 TIFF 的导出复选框 (BUG:462925)
- TIFF：修复多页文件的处理 (BUG:461975)
- TIFF：实现对分辨率单位的检测 (BUG:420932)
- EXR：实现统一的 GRAY 和 XYZ 导出 (BUG:462799)
- AVIF：添加 image/avif mimetype 到 desktop 文件，使外部程序能够了解 Krita 能够打开这种文件 (BUG:462224)
- PSD：允许大小为零的资源数据块 (BUG:461493, 450983)
- Python：修复通过 Python 新建图像 (BUG:462665)
- Python：修复使用 Document::saveAs 时更新文件名 (BUG:462667)
- Python：支持使用 Python 3.11 (BUG:461598)
- 动画：改进自动关键帧的功能 (BUG:459723)

## 下载

### 中文版信息

- 中文支持：Krita 的所有软件包均内建中文支持，首次安装时会自动设置为操作系统的语言。
- 手动设置：菜单栏 --> Settings --> Switch Application Language (倒数第二项) --> 下拉选单 --> 中文 (底部)，重启程序生效。
- G'MIC 滤镜中文翻译：打开 G'MIC 滤镜 -> 设置 (左下角) -> 勾选“Translate Filters”。

### Windows 版本

- **64 位 Windows 安装程序** [本站下载](https://download.kde.org/stable/krita/5.1.4/krita-x64-5.1.4-setup.exe) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)
- **64 位 Windows 免安装包** [本站下载](https://download.kde.org/stable/krita/5.1.4/krita-x64-5.1.4.zip) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)
- **64 位程序崩溃调试包** [本站下载](https://download.kde.org/stable/krita/5.1.4/krita-x64-5.1.4-dbg.zip) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)

- **配套资源** [中文离线文档](https://share.weiyun.com/Dea2uj0M) | [FFmpeg 软件包](https://share.weiyun.com/HzkXn7YS) | [Krita 配置文件和资源清理工具](https://share.weiyun.com/SCCloC47)

- 32 位支持：最后一版支持 32 位 Windows 的 Krita 为 4.4.3，[本站下载](https://download.kde.org/stable/krita/4.4.3/krita-x86-4.4.3-setup.exe) | [网盘下载](https://share.weiyun.com/wdMnx1WB)。
- 免安装包：解压到任意位置，运行目录中的 Krita 快捷方式。不带文件管理器缩略图插件，与已安装版本共用配置文件和资源，但程序本身相互独立。
- 程序崩溃调试包：解压到 Krita 的安装目录，在报告程序崩溃问题时用于获取回溯追踪数据。日常使用无需下载此包。

### Linux 版本

- **64 位 Linux AppImage 程序包** [本站下载](https://download.kde.org/stable/krita/5.1.4/krita-5.1.4-x86_64.appimage) | [网盘下载](https://share.weiyun.com/j7Vrjx2m)
- G'Mic 插件已经整合到主程序包中
- 如果浏览器把链接作为文本打开，请右键点击链接另存为文件。

### macOS 版本

- **macOS 程序映像** [本站下载](https://download.kde.org/stable/krita/5.1.4/krita-5.1.4.dmg) | [网盘下载](https://share.weiyun.com/jc82ykle)

- 如果您还在使用 OSX Sierra 或者 High Sierra，请[观看此视频](https://www.youtube.com/watch?v=3py0kgq95Hk)了解如何启动由开发人员签名的可执行软件包。

### 安卓版本

- **64 位 Intel CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.1.4/krita-x86_64-5.1.4-release-signed.apk) | [网盘下载](https://share.weiyun.com/KXRP4Ec0)
- **32 位 Intel CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.1.4/krita-x86-5.1.4-release-signed.apk) | [网盘下载](https://share.weiyun.com/KXRP4Ec0)
- **64 位 ARM CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.1.4/krita-arm64-v8a-5.1.4-release-signed.apk) | [网盘下载](https://share.weiyun.com/AxnO4CZZ)
- **32 位 ARM CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.1.4/krita-armeabi-v7a-5.1.4-release-signed.apk) | [网盘下载](https://share.weiyun.com/AxnO4CZZ)
- Krita 在 ChromeOS 下已经可以稳定用于生产用途，但在一般安卓系统下仍处于测试阶段。
- 安卓版的整体功能与桌面版本几乎完全相同，它尚未对手机等屏幕狭长的设备进行优化，也尚未开发平板专属的模式。建议搭配键盘使用。
- 近期上市的安卓平板一般采用 ARM CPU，Chromebook 等上网本一般采用 Intel CPU。
- 请先尝试 64 位的两种安装包，不行的话再尝试 32 位的两种。

### 源代码

- [TAR.GZ 格式源代码包](https://download.kde.org/stable/krita/5.1.4/krita-5.1.4.tar.gz)
- [TAR.XZ 格式源代码包](https://download.kde.org/stable/krita/5.1.4/krita-5.1.4.tar.xz)

### md5sum 校验码

适用于上述所有软件包，用于校验下载文件的完整性。如果您不了解文件校验，忽略即可：

- 请访问[下载目录页面](https://download.kde.org/stable/krita/5.1.4)，点击最右列的“Details”获取哈希值。

### 文件完整性验证密钥

Linux 的 Appimage 可执行文件包和源代码的 .tar.gz 和 .tar.xz tarballs 压缩包已经经过数字签名。您可以[在此下载公钥](https://files.kde.org/krita/4DA79EDA231C852B)，还可以在此下载[数字签名的 SIG 文件](https://download.kde.org/stable/krita/5.1.4/)。
