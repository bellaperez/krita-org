---
title: "Krita 5.1.0 发布候补第 1 版已经推出"
date: "2022-08-04"
categories: 
  - "news-zh"
  - "development-builds-zh"
---

今天我们为大家带来了 Krita 5.1.0 的第一个发布候补版。

完整的更新内容请见 Krita 5.1 系列版本说明 ([中文](https://krita.org/zh/krita-5-1-release-notes-zh/) | [英文](https://krita.org/en/krita-5-1-release-notes/)) (尚在编写中)。

您还可以在下方观看 Wojtek Trybus 为此版软件制作的介绍视频 (中国大陆用户需要科学上网)：

{{< youtube  TnvCjziCUGI >}}

 



![](images/2021-11-16_kiki-piggy-bank_krita5.png) Krita 是一个自由开源的软件项目，如果条件允许，请考虑通过[捐款](https://fund.krita.org)或[购买视频教程](https://krita.org/en/shop/)等方式为我们提供资金支持，这可以确保 Krita 的核心开发团队成员为项目全职工作。

## 已知问题

请注意：先前发布的 Krita 5.1 的公开测试第 1 版存在一个程序缺陷，它会造成笔刷预设的保存异常。其他版本的 Krita 在加载受影响的笔刷预设时将会崩溃。如果您遇到了此问题，请使用此 [Python 脚本](https://invent.kde.org/tymond/brushes-metadata-fixer)来修复有问题的笔刷预设。

## 新增程序缺陷修复

Krita 5.1 发布候补第 1 版在[公开测试第 2 版](https://krita.org/zh/item/krita-5-1-beta-2-zh/)的基础上又修复了以下程序缺陷：

- 修复了重命名资源的已有标签和系统标签时的一些问题。[BUG:453831](https://bugs.kde.org/show_bug.cgi?id=453831)
- Python 脚本编程：现在程序能够获取所有图案资源了。
- 修复了在笔刷预设编辑器中将动态传感器绑定到旋转选项时的拖慢现象。[BUG:456668](https://bugs.kde.org/show_bug.cgi?id=456668)
- 修复了显示器缩放情景下的色彩空间浏览器的界面缩放。[BUG:456929](https://bugs.kde.org/show_bug.cgi?id=456929)
- 修复了分镜头脚本面板的一处内存泄漏。[BUG:456998](https://bugs.kde.org/show_bug.cgi?id=456998)
- 修复了图块引擎中的一处内存泄漏。[BUG:456998](https://bugs.kde.org/show_bug.cgi?id=457998)
- 将半调滤镜的默认图案生成器设为透明度模式。
- 改进了动画在仅导出帧或者仅导出视频之间切换时的处理方式。[BUG:443105](https://bugs.kde.org/show_bug.cgi?id=443105)
- 在导出动画到帧被取消后清除生成的文件。[BUG:443105](https://bugs.kde.org/show_bug.cgi?id=443105)
- 修复了 SVG 矢量图形功能中将椭圆转换为贝塞尔曲线时会造成 Krita 耗尽内存的问题。[BUG:456922](https://bugs.kde.org/show_bug.cgi?id=456922), [BUG:439145](https://bugs.kde.org/show_bug.cgi?id=439145)
- 修复了处理矢量对象的渐变填充时的处理问题。[BUG:456807](https://bugs.kde.org/show_bug.cgi?id=456807)
- 确保 MIME 类型选择器的确定和取消按钮能被正确翻译。[BUG:448343](https://bugs.kde.org/show_bug.cgi?id=448343)
- 修复了在通过 Python 脚本编程 API 进行图像克隆时的崩溃。[BUG:457080](https://bugs.kde.org/show_bug.cgi?id=457080)
- 当创建已有图像的副本时重置文件路径。[BUG:457081](https://bugs.kde.org/show_bug.cgi?id=457081)
- 修复了复制粘贴一个带有矢量图层的图层组时的崩溃。[BUG:457154](https://bugs.kde.org/show_bug.cgi?id=457154)
- 修复了带有文字对象的图像为 PSD。[BUG:455988](https://bugs.kde.org/show_bug.cgi?id=455988)
- 修复了在撤销创建文字形状时的崩溃。[BUG:457125](https://bugs.kde.org/show_bug.cgi?id=457125)
- 修复了在为带有内嵌资源的 PSD 文件加载缩略图时的崩溃。[BUG:457123](https://bugs.kde.org/show_bug.cgi?id=456123)
- 改进了最近图像列表在从 PSD 文件创建缩略图时的性能。[BUG:456907](https://bugs.kde.org/show_bug.cgi?id=456907)
- 修复了加载带有内嵌图案的 PSD 文件。
- 修复了撤销删除标签的功能。[BUG:440337](https://bugs.kde.org/show_bug.cgi?id=440337)
- 支持打开某些无效的 PSD 文件。[BUG:444844](https://bugs.kde.org/show_bug.cgi?id=444844)
- 修复了在从 G'Mic 插件返回时的图层位置。[BUG:456950](https://bugs.kde.org/show_bug.cgi?id=456950)
- 修复了加载由 Substance Designer 创建的 JPEG-XL文件。[BUG:456738](https://bugs.kde.org/show_bug.cgi?id=456738)
- 修复了拾色器工具的工具选项中的透明度通道的显示。
- 修复了剪贴板中图像损坏时的崩溃。[BUG:456778](https://bugs.kde.org/show_bug.cgi?id=456778)
- 修复了粘贴多张参考图像时的位置。[BUG:456382](https://bugs.kde.org/show_bug.cgi?id=456382)
- 修复了在使用相连选区工具时对双击过快的处理。[BUG:450577](https://bugs.kde.org/show_bug.cgi?id=450577)
- 修复了在使用拾色器拾取颜色时对当前色板的更新。[BUG:455203](https://bugs.kde.org/show_bug.cgi?id=455203)
- 改进了笔刷速度传感器的计算。[BUG:453401](https://bugs.kde.org/show_bug.cgi?id=453401)

## 下载

### 中文版信息

- 中文支持：Krita 的所有软件包均内建中文支持，首次安装时会自动设置为操作系统的语言。
- 手动设置：菜单栏 --> Settings --> Switch Application Language (倒数第二项) --> 下拉选单 --> 中文 (底部)，重启程序生效。
- 插件翻译：G'MIC 插件[需要下载翻译包](https://share.weiyun.com/SBopNjOn)
- 网盘下载：请在官网下载困难时使用，更新时间可能会略有滞后。

### Windows 版本

- **64 位 Windows 安装程序** [本站下载](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-x64-5.1.0-RC1-setup.exe) | [网盘下载](https://share.weiyun.com/60HLzj6I)
- **64 位 Windows 免安装包** [本站下载](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-x64-5.1.0-RC1.zip) | [网盘下载](https://share.weiyun.com/60HLzj6I)
- **64 位程序崩溃调试包** [本站下载](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-x64-5.1.0-RC1-dbg.zip) | [网盘下载](https://share.weiyun.com/60HLzj6I)
- **配套网盘资源 (中文社区维护)** [中文离线文档](https://share.weiyun.com/Dea2uj0M) | [FFmpeg 软件包](https://share.weiyun.com/6tH13bVC) | [G'Mic 滤镜汉化](https://share.weiyun.com/SBopNjOn) |

- 32 位支持：最后一版支持 32 位 Windows 的 Krita 为 4.4.3，[本站下载](https://download.kde.org/stable/krita/4.4.3/krita-x86-4.4.3-setup.exe) | [网盘下载](https://share.weiyun.com/wdMnx1WB)。
- 免安装包：解压到任意位置，运行目录中的 Krita 快捷方式。不带文件管理器缩略图插件，与已安装版本共用配置文件和资源，但程序本身相互独立。
- 程序崩溃调试包：解压到 Krita 的安装目录，在报告程序崩溃问题时用于获取回溯追踪数据。日常使用无需下载此包。

### Linux 版本

- **64 位 Linux AppImage 程序包** [本站下载](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-5.1.0-RC1-x86_64.appimage) | [网盘下载](https://share.weiyun.com/C0gZ6joR)
- G'Mic 插件已经整合到主程序包中
- 如果浏览器把链接作为文本打开，请右键点击链接另存为文件。

### macOS 版本

- **macOS 程序映像** [本站下载](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-5.1.0-RC1.dmg) | [网盘下载](https://share.weiyun.com/gVg0CI53)

- 如果您还在使用 OSX Sierra 或者 High Sierra，请[观看此视频](https://www.youtube.com/watch?v=3py0kgq95Hk)了解如何启动由开发人员签名的可执行软件包。

### 安卓版本

注意：由于 Google 近期更改了安卓 SDK 的需求条件，我们这次来不及构建 Krita 5.1.0 RC1 的软件包。我们会在正式版发布时构建它们，目前请暂时继续使用[公开测试第 2 版](https://krita.org/zh/item/krita-5-1-beta-2-zh/)。

### 源代码

- [TAR.GZ 格式源代码包](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-5.1.0-RC1.tar.gz)
- [TAR.XZ 格式源代码包](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-5.1.0-RC1.tar.xz)
