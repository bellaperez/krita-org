---
title: "Krita 5.0.6 正式版已经推出"
date: "2022-04-27"
categories: 
  - "news-zh"
  - "officialrelease-zh"
---

今天我们为大家带来了 Krita 5.0.6 版。此版软件修复了以下两个与程序崩溃有关的缺陷：

- 修复了使用矢量图层或矢量选区时进行大量撤销操作时会导致程序崩溃的问题：[BUG:447985](https://bugs.kde.org/show_bug.cgi?id=447985)
- 修复了删除带有动画透明度蒙版的矢量图层时会导致程序崩溃的问题：[BUG:452396](https://bugs.kde.org/show_bug.cgi?id=452396)

![](images/2021-11-16_kiki-piggy-bank_krita5.png) Krita 是一个自由开源的软件项目，如果条件允许，请考虑通过[加入开发基金](https://fund.krita.org/)、[捐款](https://krita.org/en/support-us/donations/)或[购买视频教程](https://krita.org/en/shop/)等方式为我们提供资金支持。您的支持可以确保 Krita 的核心开发团队成员能够为 Krita 全职工作。

## 下载

### 中文版信息

- 中文支持：Krita 的所有软件包均内建中文支持，首次安装时会自动设置为操作系统的语言。
- 手动设置：菜单栏 --> Settings --> Switch Application Language (倒数第二项) --> 下拉选单 --> 中文 (底部)，重启程序生效。
- 插件翻译：G'MIC 插件[需要下载翻译包](https://share.weiyun.com/SBopNjOn)
- 网盘下载：请在官网下载困难时使用，更新时间可能滞后。

### Windows 版本

- **64 位 Windows 安装程序** [本站下载](https://download.kde.org/stable/krita/5.0.6/krita-x64-5.0.6-setup.exe) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)
- **64 位 Windows 免安装包** [本站下载](https://download.kde.org/stable/krita/5.0.6/krita-x64-5.0.6.zip) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)
- **64 位程序崩溃调试包** [本站下载](https://download.kde.org/stable/krita/5.0.6/krita-x64-5.0.6-dbg.zip) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)

- **配套网盘资源 (中文社区维护)** [中文离线文档](https://share.weiyun.com/Dea2uj0M) | [FFmpeg 软件包](https://share.weiyun.com/6tH13bVC) | [G'Mic 滤镜汉化](https://share.weiyun.com/SBopNjOn) | [Krita 配置文件和资源清理工具](https://share.weiyun.com/SCCloC47)

- 32 位支持：最后一版支持 32 位 Windows 的 Krita 为 4.4.3，[本站下载](https://download.kde.org/stable/krita/4.4.3/krita-x86-4.4.3-setup.exe) | [网盘下载](https://share.weiyun.com/wdMnx1WB)。
- 免安装包：解压到任意位置，运行目录中的 Krita 快捷方式。不带文件管理器缩略图插件，与已安装版本共用配置文件和资源，但程序本身相互独立。
- 程序崩溃调试包：解压到 Krita 的安装目录，在报告程序崩溃问题时用于获取回溯追踪数据。日常使用无需下载此包。

### Linux 版本

- **64 位 Linux AppImage 程序包** [本站下载](https://download.kde.org/stable/krita/5.0.6/krita-5.0.6-x86_64.appimage) | [网盘下载](https://share.weiyun.com/j7Vrjx2m)
- G'Mic 插件已经整合到主程序包中
- 如果浏览器把链接作为文本打开，请右键点击链接另存为文件。

### macOS 版本

- **macOS 程序映像** [本站下载](https://download.kde.org/stable/krita/5.0.6/krita-5.0.6.dmg) | [网盘下载](https://share.weiyun.com/jc82ykle)

- 如果您还在使用 OSX Sierra 或者 High Sierra，请[观看此视频](https://www.youtube.com/watch?v=3py0kgq95Hk)了解如何启动由开发人员签名的可执行软件包。

### 安卓版本

- **64 位 Intel CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.0.6/krita-x86_64-5.0.6-release-signed.apk) | [网盘下载](https://share.weiyun.com/KXRP4Ec0)
- **32 位 Intel CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.0.6/krita-x86-5.0.6-release-signed.apk) | [网盘下载](https://share.weiyun.com/KXRP4Ec0)
- **64 位 ARM CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.0.6/krita-arm64-v8a-5.0.6-release-signed.apk) | [网盘下载](https://share.weiyun.com/AxnO4CZZ)
- **32 位 ARM CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.0.6/krita-armeabi-v7a-5.0.6-release-signed.apk) | [网盘下载](https://share.weiyun.com/AxnO4CZZ)
- Krita 现在已经能够在 ChromeOS 下正常运行，但其他安卓系统的支持目前尚处于测试阶段。
- 安卓版的整体功能与桌面版本几乎完全相同，它尚未对手机等屏幕狭小细长的设备进行优化，也尚未开发平板专属的模式，因此建议搭配键盘使用。
- 请先尝试 64 位的两种安装包，不行的话再尝试 32 位的两种。
- 比较新款的安卓平板一般使用 ARM CPU，Chromebook 等一般使用 Intel CPU。

### 源代码

- [TAR.GZ 格式源代码包](https://download.kde.org/stable/krita/5.0.6/krita-5.0.6.tar.gz)
- [TAR.XZ 格式源代码包](https://download.kde.org/stable/krita/5.0.6/krita-5.0.6.tar.xz)

### md5sum 校验码

适用于上述所有软件包，用于校验下载文件的完整性。如果您不了解文件校验，忽略即可：

- 请访问[下载目录页面](https://download.kde.org/stable/krita/5.0.6)，点击最右列的“Details”获取哈希值。

### 文件完整性验证密钥

Linux 的 Appimage 可执行文件包和源代码的 .tar.gz 和 .tar.xz tarballs 压缩包已经经过数字签名。您可以[在此下载公钥](https://files.kde.org/krita/4DA79EDA231C852B)，还可以在此下载[数字签名的 SIG 文件](https://download.kde.org/stable/krita/5.0.6)。
