---
title: "Krita Monthly Update – Edition 13"
date: "2024-03-09"
---


Welcome to all krita artists, this monthly zine is curated for you by the [Krita-promo](https://krita-artists.org/g/krita-promo) team.


## Development report

- **Changes to KDE Binary Factory - Krita Next and Krita Plus builds**

  Nightly builds for Windows and Linux have been moved to GitLab. Binary Factory is now decommissioned. Due to this change the nightly build service is temporarily discontinued. The developers are working on getting the build up again.

- **New Krita website is released.**

  The work for the new website was ongoing for some time so we are glad to [announce](/posts/2024/new-krita-site) that it is live now. The new website offers a light and dark theme. It is cleaner and the translation to other languages is much easier now. We are always working to improve the website so if you find any rough edges please let us know.

- **Internal Roadmap for Krita**

  The developers had an online meeting on 26th February to discuss the future path for Krita development. Stay tuned for an upcoming blog post here for more details about this meeting. In the meantime, enjoy these meeting highlights.  The agenda for the meeting was:
    - **How to handle social networks and having a social media strategy.**

      Krita’s social media presence was handled by the developers earlier, but since they are busy with Krita’s development, we can request volunteers to help us. Krita-Artists group of volunteers can be asked to handle social media posting and any volunteers are welcome to join the group.

    - **Challenges and feasibility of keeping the support for Android version.**

      The person who was handling the support for the Android version has gotten busy with life so currently there is no one to look after it. The builds are also stopped due to our build server getting decommissioned. Dmitry is looking into the automated build issue but the team needs a way to keep the support up. There may be close to 500,000 users of Krita on this platform. Volunteers are more than welcome to join us in this endeavour.

    - **Various other aspects related to development**
      * The developers discussed some features that can be implemented such as audio waveform support in the animation timeline and the future path for creating a mobile UI.
      * A Strategy for porting Krita to the next version of Qt (Qt is the underlying base that is used to build Krita).
      * Areas where GPU computation can help. Artists who joined the meeting said that filters and transform masks were slow in krita. Our Liquify tool also needs a performance boost. So GPU utilisation in this area is welcome.
      * Tiar will be investigating how to do AI assisted Inking. **Disclaimer** - this doesn’t mean we will be using the popular AI models out there. We intend to do this ethically and as this is still in the initial investigation stage, the developers are still discussing various aspect about how to approach this subject.
      * How to handle PS style clipping mask - Deif Lou has done an awesome job in researching and investigating the clipping mask, layer effects and blending mode technicalities. The team intends to look into this and tackle this feature together.

- **New features that got merged this month**

  - **Close Gap in fill tool is finally here!**

    [YRH](https://krita-artists.org/u/yrh/summary) created a gap-closing patch for the fill tool and that patch has been accepted to master. In this post, YRH points out that Dmitry and Krita users on this forum were instrumental in getting this done. You can read these latest comments and get the test builds from [this post](https://krita-artists.org/t/close-gap-in-fill-tool/32043/210?u=sooz).

    {{< video-player src="videos/posts/2024/deevad-close-gap-in-fill.mp4" type="video/mp4" >}}
    (Video created by David Revoy)

  - **Text tool on-canvas basic rich text editing**

    Wolthera has been busy with text tool for some time now. You can tell by the text tool update [thread](https://krita-artists.org/t/text-tool-thread/57973) that she is merging really exciting things one after the other. This month, Krita got support for on-canvas text editing with basic rich text support. As kaichi1342 reports on the forum, currently common shortcuts like Ctrl B, I, U for bold italics and underline are working, full and partial color change of text works on canvas.

    {{< video-player src="videos/posts/2024/wolthera-rich-text-editing.mp4" type="video/mp4" >}}
    (Video created by Wolthera)

  - **Docker support added to popup palette**

    Freyalupen implemented docker support in the right click popup palette which can be of immense help for people who work on minimal canvas-only mode or for people using Krita on tablets. You can now use various dockers like the layer docker, brush preset history, etc., right from the right click popup palette.

    {{< video-player src="videos/posts/2024/freyalupen-popup-palette-dockers4.mp4" type="video/mp4" >}}
    (Video created by freyalupen)

## Community report

### Monthly Art Challenge

Krita-Artists’ Monthly Art Challenge is a great way to stretch your skills and learn more about Krita.

February’s Art Challenge theme was Architectural/Urban, designed by [Elixiah](https://krita-artists.org/u/elixiah/summary). We had a full slate of submissions to vote on at the end of the month. [Mythmaker](https://krita-artists.org/u/mythmaker/summary) won the challenge with this image:

![Entry by myhtmaker for monthly art challenge on KA](images/posts/2024/monthly-contest-winner-entry-mythmaker.jpeg)
The challenge for this month is [Marvellous Metal](https://krita-artists.org/t/monthly-art-challenge-march-2024-marvellous-metal/85832). Why not join in? It’s a friendly competition where we even share tips and help each other with challenge submissions on the [WIP thread](https://krita-artists.org/t/monthly-art-challenge-wips-and-discussion-thread-march-2024/85836/12).

### YouTube Growth

The [Krita YouTube channel](https://www.youtube.com/c/KritaOrgPainting) has reached 80,000 subscribers. That’s a gain of 17,000 subs in [just over a year](https://krita-artists.org/t/krita-weekly-update-january-2023-week-4/56799). Ramon’s most recent video, [5.2.2 New Features](https://youtu.be/t81PkQwf7NM?si=OsG2AXYPUEYT7uWR), has already had more than 86,000 views over the last month.

{{< youtube t81PkQwf7NM >}}

## Featured artwork

### Introducing “Best of Krita-Artists” Featured Artwork Nomination Process

![featured image row on KA](images/posts/2024/featured-row.jpeg)

**Great news**: Members Hall and the nomination process is now open to all Krita-Artists members. Everyone has the opportunity to nominate artwork for the featured gallery. Monthly submission threads will open on the 15th of each month. We’ll use your submissions to create a poll which will determine the top four. The winning images will be added to the featured gallery.



The current [instructions and submission thread](https://krita-artists.org/t/best-of-krita-artists-february-2024-submissions-thread/84907) explains everything you need to know in order to nominate artwork that you feel represents the best of Krita-Artists. In January, we’ll create an annual poll to vote for the very best from 2024.

## Noteworthy plugin

[Shortcut Composer v1.5.0 Released](https://krita-artists.org/t/shortcut-composer-v1-5-0-plugin-for-pie-menus-multiple-key-assignment-mouse-trackers-and-more/55314) (this update requires Krita 5.2.2 or higher)

Highlights of new features:
* New action: Rotate brush which rotates the brush tip of the current preset
* New action: Rotate canvas
* Tooltips with additional info that appear when hovering over settings

![|Screenshot of the plugin in action](images/posts/2024/shortcut_composer_screenshot.png)

## Tutorial of the month

From David Revoy: Grayscale to Color – Character Design
“A commented step-by-step guide and advice on how to paint an original fantasy character design from scratch in Krita.”

{{< youtube Q7kMT78iBSg >}}

## Notable changes in code

This section has been compiled by [[freyalupen]](https://krita-artists.org/u/freyalupen/summary).
(Feb 5 - Mar 5, 2024)

Stable branch (5.2.2+):
Bugfixes:

* [Animation] Fix framerate resetting to 24 after adding audio or reopening the animation. ([BUG:481388](https://bugs.kde.org/481388)) ([commit, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/commit/8ece4acc96))
* [Animation] Fix drawing on a paused animation to draw on the shown frame and without visual glitches. ([BUG:481244](https://bugs.kde.org/481244)) ([commit, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/commit/9b38692174))
* [Animation] Fix added hold frames stopping playback until cache gets updated. ([BUG:481099](https://bugs.kde.org/481099)) ([merge request, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/merge_requests/2082))
* [Assistant Tool] Small fixes for snapping on stroke start and other Assistant issues. ([merge request, Mathias Wein](https://invent.kde.org/graphics/krita/-/merge_requests/2066))

Stable branch (5.2.2+) backports from Unstable:
Bugfixes:

* Backport several older fixes from the 5.3 branch to the 5.2 branch, after they've had time to be tested for stability. This includes many fixes related to animation and/or transformation ([BUG:475385](https://bugs.kde.org/475385), [BUG:475334](https://bugs.kde.org/475334), [BUG:444791](https://bugs.kde.org/), [BUG:456731](https://bugs.kde.org/), [BUG:476317](https://bugs.kde.org/), [BUG:478966](https://bugs.kde.org/444791), [BUG:478448](https://bugs.kde.org/478448), [BUG:479664](https://bugs.kde.org/479664), [BUG:475745](https://bugs.kde.org/475745)), but also fixes to colorize mask ([BUG:475927](https://bugs.kde.org/475927)), adjustment layers ([BUG:473853](https://bugs.kde.org/473853)), canvas inputs ([BUG:451424](https://bugs.kde.org/451424)), colorpicking from labelled layers ([BUG:471896](https://bugs.kde.org/471896), [BUG:472700](https://bugs.kde.org/472700)), and the ability to use the Wave filter as a mask ([BUG:476033](https://bugs.kde.org/476033)). ([merge request, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/merge_requests/2086))

---

Unstable branch (5.3.0-prealpha):
Features:

* [Text Tool] Implement basic rich text editing in the on-canvas text tool. This includes changing the color with the color selectors, setting bold/italic/underline with keyboard shortcuts, and rich text copy/paste. ([merge request, Wolthera van Hövell](https://invent.kde.org/graphics/krita/-/merge_requests/2067))
* [Fill Tool] Implement 'Close Gap' option in the Fill Tool and Contiguous Selection Tool. This allows the unleaked filling of gapped lineart by treating gaps of a configured size as if they were closed. ([merge request, Maciej Jesionowski](https://invent.kde.org/graphics/krita/-/merge_requests/2050))
* [Popup Palette, Dockers] Add ability to show dockers, such as the Layers docker, in the Popup Palette's side panel. The On-Canvas Brush Editor that was in this panel is now a docker. ([merge request, Freya Lupen](https://invent.kde.org/graphics/krita/-/merge_requests/2062))
* [Brush Engines] Add Photoshop-like brush texturing modes where Strength affects the texture instead of the dab, enabled with the 'Soft texturing' checkbox in the brush Pattern Options. ([merge request, Deif Lou](https://invent.kde.org/graphics/krita/-/merge_requests/2068))
* [File Formats: JPEG-XL] Update libjxl and add options to export JPEG-XL with CICP profile and lossless alpha. ([merge request, Rasyuqa A H (Kampidh)](https://invent.kde.org/graphics/krita/-/merge_requests/2077))
* [Grids and Guides Docker] Add button to delete all guides. ([merge request, reinold rojas](https://invent.kde.org/graphics/krita/-/merge_requests/2078))
* [Animation: Onion Skins Docker] Add Reset option for Onion Skins' opacity in a right-click menu, to reset them to the default values. ([WISHBUG:466977](https://bugs.kde.org/466977)) ([commit, Emmet O'Neill](https://invent.kde.org/graphics/krita/-/commit/e36d761cd2))

Bugfixes:

* [Assistant Tool] Re-enable assistant preview for the eclipse, rectangle, polygon, and polyline tools. ([merge request, reinold rojas](https://invent.kde.org/graphics/krita/-/merge_requests/2073))
* [Layer Stack] Fix a bug which emitted a warning on undoing flattening a group. ([BUG:474122](https://bugs.kde.org/474122)) ([merge request, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/merge_requests/2070))

---

These changes are made available for testing in the latest development builds:

* Stable "Krita Plus" (5.2.2+): [Pipeline List](https://invent.kde.org/graphics/krita/-/pipelines?page=1&scope=all&ref=krita%2F5.2&status=success) | Download Latest: [Linux](https://invent.kde.org/api/v4/projects/206/jobs/artifacts/krita/5.2/download?job=linux-build) - [Windows](https://invent.kde.org/api/v4/projects/206/jobs/artifacts/krita/5.2/download?job=windows-build)
* Unstable "Krita Next" (5.3.0-prealpha): [Pipeline List](https://invent.kde.org/graphics/krita/-/pipelines?page=1&scope=all&ref=master&status=success) | Download Latest: [Linux](https://invent.kde.org/api/v4/projects/206/jobs/artifacts/master/download?job=linux-build) - [Windows](https://invent.kde.org/api/v4/projects/206/jobs/artifacts/master/download?job=windows-build)

(macOS and Android builds will be available in the future.)

---

## Ways to help Krita

Krita is a Free and Open Source application, mostly developed by an international team of enthusiastic volunteers. Donations from Krita users to support maintenance and development is appreciated. Join the [Development Fund](https://fund.krita.org/) with a monthly donation. Or make a one-time donation [here](https://krita.org/en/donations/).
![donate to krita](images/pages/2021-11-16_kiki-piggy-bank_krita5.png)


