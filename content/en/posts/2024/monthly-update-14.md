---
title: "Krita Monthly Update – Edition 14"
date: "2024-04-04"
---

Welcome to the latest development and community news curated for you by the [Krita-promo](https://krita-artists.org/g/krita-promo) team.

## Development report

**Official Recap of February’s Online Development Meeting**

Last month we provided highlights of the video meeting led by Halla, Krita’s Maintainer. She has since published a [post](/posts/2024/2024-roadmap)  on Krita.org presenting the challenges and opportunities that came out of that meeting.

One of the largest projects this year is porting Krita from Qt5 to Qt6 (Qt is the framework upon which Krita is built). This is a major change and will require serious development time. We invite you to read more about the considerations of this project as well as other ideas the Krita team is currently discussing and changes that have taken place within Krita’s development team. You can access it [here](https://krita.org/en/posts/2024/2024-roadmap/?pk_kwd=WhatWe%27reUpToIn2024).

## Highlights of this month

* [YRH](https://krita-artists.org/u/yrh/summary) tackled a [feature request](https://krita-artists.org/t/the-canvas-should-not-be-shifted-with-the-panels-hiding/86429) to prevent the canvas from shifting position when toggling canvas-only mode. [Deif_Lou](https://krita-artists.org/u/deif_lou) assisted by also merging a fix addressing a jump in canvas position.
* [Grum999](https://krita-artists.org/u/grum999) identified an opportunity to improve grids and guides management such that settings are now saved in Krita documents. The grid offset on/off toggle was improved so that user settings are retained. Isometric grids may now be measured more accurately (Note: the original code was preserved and is now called “Isometric Legacy” to ensure compatibility with older documents).
* The most recent text tool merge from [Wolthera](https://krita-artists.org/u/wolthera) for 5.3 means Krita can now store units (relative units for letter spacing and font size). Wolthera reports that about half the properties are now implemented.

    {{< video-player src="videos/posts/2024/text-tool-demo-march-properties-2024-03-14_10.21.53.mp4" type="video/mp4" >}}
    (Video created by Wolthera)

* Stable and unstable nightly builds are back following the migration to GitLab CI, with the exception of macOS which is being worked on. On Android, Krita will no longer be built for the 32-bit x86 architecture.

## Community report

### March 2024 Monthly Art Challenge
[Krita-Artists](https://krita-artists.org) members outdid themselves by creating 41 images for [Mythmaker’s](https://krita-artists.org/u/mythmaker/activity/portfolio) challenge: Marvellous Metal. The quality and calibre of the entries made it tough to choose only two when it was time to vote. [Elixiah](https://krita-artists.org/u/elixiah/activity/portfolio) emerged the winner with these two images:

{{< figure src="images/posts/2024/Vintage-Forgotten-Ford-elixiah.jpeg" title="Vintage Forgotten Ford by elixiah" >}}

{{< figure src="images/posts/2024/Wildkat-Engine-elixiah.jpeg" title="Wildkat-Engine by elixiah" >}}


Elixiah asked [MangooSalade](https://krita-artists.org/u/mangoosalade/activity/portfolio) and [jimplex](https://krita-artists.org/u/jimplex/activity/portfolio), who tied for second place, to choose the April challenge and they have come up with a good one! Our new topic is Animal Curiosity and this time there is an additional challenge. Read all about it [here](https://krita-artists.org/t/monthly-art-challenge-april-2024-topic-animal-curiosity-challenge-limited-palette/88235).

**We’re Asking for Ideas**

Mythmaker started a very [positive conversation](https://krita-artists.org/t/a-better-feature-awards-system-your-thoughts-and-ideas-needed/87195) about improving the way images are selected for the featured artwork banner on krita-artists.org website. We have had nomination processes in place for some time which have been somewhat effective (and you’ll see the results of our first featured artwork poll in the next section) but we haven’t landed on a system that makes it easy to nominate artwork and is manageable administratively. Take a glance at some of the proposed ideas – something in there might trigger a new idea for you to share.

## Featured artwork

We held our very first [monthly poll](https://krita-artists.org/t/best-of-krita-artists-february-march-2024-submissions-thread/84907/19?u=sooz) for the Krita-Artists featured artwork banner. Five images were added to the banner so thank you for nominating and voting. [Dragon Courier](https://krita-artists.org/t/dragon-courier/84137) by [desenhunos](https://krita-artists.org/u/desenhunos/activity/portfolio) was the #1 pick.

![dragon-courier-by-desenhunos](images/posts/2024/dragon-courier-by-desenhunos.jpeg)

The March/April nomination thread will be open until April 10, 2024. [Here’s how you can participate](https://krita-artists.org/t/best-of-krita-artists-march-april-2024-nomination-submissions-thread/87070).

## Noteworthy plugin

**HCL Sliders by Lucifer**
“HCL Sliders is a color slider plugin with various hue/colorfulness/lightness models for use with the sRGB color profile and its linear counterpart.” More details about the plugin’s capabilities can be found in [Lucifer’s post](https://krita-artists.org/t/hcl-sliders/88250).

![hclsliders](images/posts/2024/hcl-plugin.jpeg)


## Tutorial of the month

**Wrap Around Mode by David Revoy**
In just two minutes, you’ll learn what makes this feature so powerful.

{{< youtube acvr_uqn__g >}}


## Notable changes in code

This section has been compiled by [freyalupen](https://krita-artists.org/u/freyalupen/summary).
Mar 6 - Mar 31, 2024

---

### Stable branch (5.2.2+):

**Bugfixes**:

* **File Formats: PNG** -  Fix lines in export of 32-bit CMYK images to PNG, by preventing multiple color conversions. ([BUG:475737](https://bugs.kde.org/475737)) ([merge request, Rasyuqa A H (Kampidh)](https://invent.kde.org/graphics/krita/-/merge_requests/2101))
* **Usability** -  Fix a jump in canvas position when panning after going to Canvas-Only mode. ([commit, Deif Lou](https://invent.kde.org/graphics/krita/-/commit/d4c90a9a79))
* **Linux: KDE Plasma** -  Fix Krita's menu not appearing in Plasma's global menu. ([BUG:483170](https://bugs.kde.org/483170)) ([merge request, Halla Rempt](https://invent.kde.org/graphics/krita/-/merge_requests/2098))
* **Text Tool** -  Fix a crash when pasting font family name into rich text editor and saving changes without hitting enter first. ([BUG:484066](https://bugs.kde.org/484066)) ([merge request, Igor Danilets](https://invent.kde.org/graphics/krita/-/merge_requests/2102))

**Features**:

* **General** -  Tweak the Welcome Page. New/Open File labels moved beside the icons. Recent file thumbnails have a tinted background. ([merge request, Agata Cacko](https://invent.kde.org/graphics/krita/-/merge_requests/2047))
* **General** - Remove the development fund banner from the Welcome Page, as it was ineffective. ([commit, Halla Rempt](https://invent.kde.org/graphics/krita/-/commit/4eae3c7bf2))

---

### Unstable branch (5.3.0-prealpha):

**Features**:

* **Text** -  Allow keeping track of relative font units (em). ([merge request, Wolthera van Hövell](https://invent.kde.org/graphics/krita/-/merge_requests/2085))
* **Enclose and Fill Tool** -  Support gap closing in the Enclose and Fill tool. ([commit, Deif Lou](https://invent.kde.org/graphics/krita/-/commit/01ef0e0bbf))
* **Enclose and Fill Tool** - Add "Include contour regions" option to the "All regions" method in the Enclose and Fill tool. ([commit, Deif Lou](https://invent.kde.org/graphics/krita/-/commit/140f0f67be))
* **Vector Layers** -  Add an option to disable/enable anti-aliasing on Vector Layers. ([merge request 1](https://invent.kde.org/graphics/krita/-/merge_requests/2091), [merge request 2](https://invent.kde.org/graphics/krita/-/merge_requests/2095), Grum 999)
* **Grids and Guides Docker** -  Various improvements to Grid and Guides. Grid and Guide properties are now saved into .kra files. New type of isometric grid that ensures the cell lengths match. ([merge request, Grum 999](https://invent.kde.org/graphics/krita/-/merge_requests/2090))
* **Animation Dockers** -  Add Lock Docker button to animation dockers. ([merge request, reinold rojas](https://invent.kde.org/graphics/krita/-/merge_requests/2083))
* **Scripting** -  Add various methods to Scratchpad API related to fill, zoom, and pan. ([merge request, Grum 999](https://invent.kde.org/graphics/krita/-/merge_requests/2087))

**Bugfixes**:

* **Layer Stack**- Fix a crash when using color labeled layers as a reference for selection if color label includes a mask. ([BUG:480601](https://bugs.kde.org/480601)) ([commit, Deif Lou](https://invent.kde.org/graphics/krita/-/commit/1a2dd9786e))
* **SVG** -  Fix saving 'paint-order' tag for non-text shapes. ([commit, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/commit/05612f5749))
* **Wide Gamut Color Selector**-  Fix Wide Gamut Color Selector shortcut popup closing immediately if the cursor is moving while triggering it. ([merge request, reinold rojas](https://invent.kde.org/graphics/krita/-/merge_requests/2069))
* **Usability** -  When switching to and from Canvas Only mode, keep the canvas in the same position. ([merge request, Maciej Jesionowski](https://invent.kde.org/graphics/krita/-/merge_requests/2097))

---

These changes are made available for testing in the following builds:

* **Stable "Krita Plus" (5.2.2+)**: [Linux](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/windows) - Android ([arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm64-v8a) / [arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm32-v7a) / [x86_64](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-x86_64))
* **Unstable "Krita Next" (5.3.0-prealpha)**: [Linux](https://cdn.kde.org/ci-builds/graphics/krita/master/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/master/windows) - Android ([arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm64-v8a) / [arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm32-v7a) / [x86_64](https://cdn.kde.org/ci-builds/graphics/krita/master/android-x86_64))

(macOS builds will be available in the future.)

{{< support-krita-callout >}}
