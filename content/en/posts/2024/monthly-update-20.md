---
title: "Krita Monthly Update - Edition 20"
date: "2024-11-12"
---
Welcome to the [@Krita-promo](https://krita-artists.org/groups/krita-promo) team's October 2024 development and community update.

## Development Report
### Android-only Krita 5.2.8 Hotfix Release
Krita 5.2.6 was reported to crash on startup on devices running Android 14 or later. This was caused by issues with an SDK update required for release on the Play Store, so a temporary 5.2.7 release reverting it was available from the downloads page only.

However, the issue has now been resolved and 5.2.8 is rolling out on the Play Store. Note that 5.2.8 raises the minimum supported Android version to Android 7.0 (Nougat).

### Community Bug Hunt Started
The development team has declared a "Bug Hunt Month" running through November, and needs the community's help to decide what to do with each and every one of the hundreds of open bug reports on the bug tracker. Which reports are valid and need to be fixed? Which ones need more info or are already resolved?

Read the bug hunting guide and join in on the [bug hunt thread on the Krita-Artists forum](https://krita-artists.org/t/volunteers-needed-krita-bughunt-month-started/104916).

## Community Report
### October 2024 Monthly Art Challenge Results
For the ["Buried, Stuck, or otherwise Swallowed"](https://krita-artists.org/t/monthly-art-challenge-october-2024-topic-buried-stuck-or-otherwise-swallowed/103132/) theme, 16 members submitted 18 original artworks.
And the winner is… [Tomorrow, contest… I’m so finished](https://krita-artists.org/t/tomorrow-contest-i-m-so-finished/105731) by @mikuma_poponta!
<figure>
<a href="images/posts/2024/mu20_tomorrow_contest_im_so_finished-mikuma_poponta.jpeg"> <img class="fit" src="images/posts/2024/mu20_tomorrow_contest_im_so_finished-mikuma_poponta.jpeg" alt=" Tomorrow, contest… I’m so finished by @mikuma_poponta"/> </a>
</figure>

### The November Art Challenge is Open Now
For the November Art Challenge, @mikuma_poponta has chosen ["Fluffy"](https://krita-artists.org/t/monthly-art-challenge-november-2024-topic-fluffy/105804) as the theme, with the optional challenge of making it "The Ultimate Fluffy According to Me". See the full brief for more details, and get comfortable with this month's theme.

## Featured Artwork
### Best of Krita-Artists - September/October 2024
8 images were submitted to the [Best of Krita-Artists Nominations thread](https://krita-artists.org/t/best-of-krita-artists-september-october-2024-nomination-submissions-thread/101815/), which was open from September 14th to October 11th. When the poll closed on October 14th, moderators had to break a 
four-way tie for the last two spots, resulting in these five wonderful works making their way onto the Krita-Artists featured artwork banner:

[Sapphire](https://krita-artists.org/t/sapphire/102303) by @Dehaf
<figure>
<a href="images/posts/2024/mu20_sapphire-dehaf.jpeg"> <img class="fit" src="images/posts/2024/mu20_sapphire-dehaf.jpeg" alt="Sapphire by @Dehaf"/> </a>
</figure>

[Sci-Fi Spaceship](https://krita-artists.org/t/sci-fi-spaceship/101839) by @NAKIGRAL
<figure>
<a href="images/posts/2024/mu20_sci-fi_spaceship-nakigral.jpeg"> <img class="fit" src="images/posts/2024/mu20_sci-fi_spaceship-nakigral.jpeg" alt="Sci-Fi Spaceship by @NAKIGRAL"/> </a>
</figure>

[Oracular Oriole](https://krita-artists.org/t/oracular-oriole/103748/) by @SylviaRitter
<figure>
<a href="images/posts/2024/mu20_oracular_oriole-sylviaritter.jpeg"> <img class="fit" src="images/posts/2024/mu20_oracular_oriole-sylviaritter.jpeg" alt="Oracular Oriole by @SylviaRitter"/> </a>
</figure>

[Air Port](https://krita-artists.org/t/air-port/91762/) by @agarad
<figure>
<a href="images/posts/2024/mu20_air_port-agarad.jpeg"> <img class="fit" src="images/posts/2024/mu20_air_port-agarad.jpeg" alt="Air Port by @agarad"/> </a>
</figure>

[Dancing with butterflies 🦋](https://krita-artists.org/t/dancing-with-butterflies/103566/) by @Kichirou_Okami
<figure>
<a href="images/posts/2024/mu20_dancing_with_butterflies-kichirou_okami.jpeg"> <img class="fit" src="images/posts/2024/mu20_dancing_with_butterflies-kichirou_okami.jpeg" alt="Dancing with butterflies 🦋 by @Kichirou_Okami"/> </a>
</figure>

### Best of Krita-Artists - October/November 2024
Nominations were accepted until November 11th. The [poll is now open](https://krita-artists.org/t/best-of-krita-artists-voting-open-october-november-2024-nomination-submissions-thread/104381/10) until November 14th. Don't forgot to vote!

## Ways to Help Krita
Krita is Free and Open Source Software developed by an international team of sponsored developers and volunteer contributors.

Visit [Krita's funding page](https://krita.org/en/donations/) to see how user donations keep development going, and explore a one-time or monthly contribution. Or check out more ways to [Get Involved](https://krita.org/en/get-involved/), from testing, coding, translating, and documentation writing, to just sharing your artwork made with Krita.

The Krita-promo team has put out a [call for volunteers](https://krita-artists.org/t/request-for-krita-promo-monthly-update-volunteers/101404/), come join us and help keep these monthly updates going.

## Notable Changes
Notable changes in Krita's development builds from Oct. 10 - Nov. 12, 2024.

### Stable branch (5.2.9-prealpha):
* Layers: Fix infinite loop when a clone layer is connected to a group with clones, and a filter mask triggers an out-of-bounds update. ([Change](https://invent.kde.org/graphics/krita/-/commit/d2022e8ba4), by Dmitry Kazakov)
* General: Fix inability to save a document after saving while the image is busy and then canceling the busy operation. ([bug report](https://bugs.kde.org/496018)) ([Change](https://invent.kde.org/graphics/krita/-/commit/23882dc7a6), by Dmitry Kazakov)
* Resources: Fix crash when re-importing a resource after modifying it. ([bug report](https://bugs.kde.org/484796)) ([Change](https://invent.kde.org/graphics/krita/-/commit/b18e25c5b9), by Dmitry Kazakov)
* Brush Presets: Fix loading embedded resources from .kpp files. ([bug report](https://bugs.kde.org/487866), [bug report](https://bugs.kde.org/456586), [bug report](https://bugs.kde.org/456197)) ([Change](https://invent.kde.org/graphics/krita/-/commit/072460560b), by Dmitry Kazakov)
* Brush Tools: Fix the Dynamic Brush Tool to not use the Freehand Brush Tool's smoothing settings which it doesn't properly support. ([bug report](https://bugs.kde.org/493249)) ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2251), by Mathias Wein)([Change](https://invent.kde.org/graphics/krita/-/commit/da166935de), by Dmitry Kazakov)
* Recorder Docker: Prevent interruption of the Text Tool by disabling recording while it is active. ([bug report](https://bugs.kde.org/495768)) ([Change](https://invent.kde.org/graphics/krita/-/commit/be44e45a83), by Dmitry Kazakov)
* File Formats: EXR: Possibly fix saving EXR files with extremely low alpha values. ([Change](https://invent.kde.org/graphics/krita/-/commit/9d5e921077), by Dmitry Kazakov)
* File Formats: EXR: Try to keep color space profile when saving EXR of incompatible type. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2252), by Dmitry Kazakov)
* File Formats: EXR: Fix bogus offset when saving EXR with moved layers. ([Change](https://invent.kde.org/graphics/krita/-/commit/f8b4dce837), by Dmitry Kazakov)
* File Formats: JPEG-XL: Fix potential lockup when loading multi-page images. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2263), by Rasyuqa A H)
* Keyboard Shortcuts: Set the default shortcut for Zoom In to = instead of +. ([bug report](https://bugs.kde.org/484365)) ([Change](https://invent.kde.org/graphics/krita/-/commit/3ca29b881f), by Halla Rempt)
* Brush Editor: Make the Saturation and Value brush options' graph and graph labels consistently agree on the range being -100% to 100% with 0% as neutral. ([bug report](https://bugs.kde.org/487469)) ([Change](https://invent.kde.org/graphics/krita/-/commit/da166935de), by Dmitry Kazakov)

### Unstable branch (5.3.0-prealpha):
Bug fixes:
* Vector Layers: Fix endlessly slow rendering of vector layers with clipping masks. ([bug report](https://bugs.kde.org/458407)) ([Change](https://invent.kde.org/graphics/krita/-/commit/d01ed5efaa), by Dmitry Kazakov)
* Layers: Fix issue with transform masks on group layers not showing until visibility change, and visibility change of passthrough groups with layer styles causing artifacts. ([bug report](https://bugs.kde.org/491447)) ([Change](https://invent.kde.org/graphics/krita/-/commit/75a084ca7e), by Dmitry Kazakov)
* Brush Editor: Fix crash when clearing scratchpad while it's busy rendering a resource-intensive brushstroke. ([bug report](https://bugs.kde.org/488800)) ([Change](https://invent.kde.org/graphics/krita/-/commit/1b1422e09a), by Dmitry Kazakov)
* File Formats: EXR: Add GUI option for selecting the default color space for EXR files. ([Change](https://invent.kde.org/graphics/krita/-/commit/99ec66c1d6), by Dmitry Kazakov)
* Transform Tool: Liquify: Move the Move/Rotate/Scale/Offset/Undo buttons to their own spot instead of alongside unrelated options, to avoid confusion. ([bug report](https://bugs.kde.org/442227)) ([Change](https://invent.kde.org/graphics/krita/-/commit/898ffe3a7b), by Emmet O'Neill)
* Move Tool: Fix Force Instant Preview in the Move tool to be off by default. (CC[bug report](https://bugs.kde.org/491264)) ([Change](https://invent.kde.org/graphics/krita/-/commit/d2f533594f), by Halla Rempt)
* Pop-Up Palette: Fix lag in selecting a color in the Pop-Up Palette. ([bug report](https://bugs.kde.org/490873)) ([Change](https://invent.kde.org/graphics/krita/-/commit/1fe16cb3e5), by Dmitry Kazakov)
* Scripting: Fix accessing active node state from the Python scripts. ([bug report](https://bugs.kde.org/495811)) ([Change](https://invent.kde.org/graphics/krita/-/commit/b1994c6145), by Dmitry Kazakov)
* Usabillity: Remove unnecessary empty space at the bottom of Transform, Move and Crop tool options. ([bug report](https://bugs.kde.org/495809)) ([Change](https://invent.kde.org/graphics/krita/-/commit/7bc7be7f8c), by Dmitry Kazakov)

## Nightly Builds
Pre-release versions of Krita are built every day for testing new changes.

Get the latest bugfixes in **Stable** "Krita Plus" (5.2.9-prealpha): [Linux](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/windows) - [macOS (unsigned)](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/macos) - [Android arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm64-v8a) - [Android arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm32-v7a) - [Android x86_64](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-x86_64)

Or test out the latest **Experimental** features in "Krita Next" (5.3.0-prealpha). Feedback and bug reports are appreciated!: [Linux](https://cdn.kde.org/ci-builds/graphics/krita/master/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/master/windows) - [macOS (unsigned)](https://cdn.kde.org/ci-builds/graphics/krita/master/macos) - [Android arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm64-v8a) - [Android arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm32-v7a) - [Android x86_64](https://cdn.kde.org/ci-builds/graphics/krita/master/android-x86_64)


## Have feedback?
Join the [discussion of this post](https://krita-artists.org/t/krita-monthly-update-edition-20/106882) on the [Krita-Artists forum](https://krita-artists.org/)!
