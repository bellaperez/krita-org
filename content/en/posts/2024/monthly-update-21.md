---
title: "Krita Monthly Update - Edition 21"
date: "2024-12-16"
---
Welcome to the [@Krita-promo](https://krita-artists.org/groups/krita-promo) team's November 2024 development and community update.

## Development Report
### Community Bug Hunt Ended
The [Community Bug Hunt has ended](https://krita-artists.org/t/volunteers-needed-krita-bughunt-month-started/104916/52), with dozens of bugs fixed and over a hundred bug more reports closed. Huge thanks to everyone who participated, and if you missed it, the plan is to make this a regular occurrence.

Can't wait for the next bug hunt to be scheduled? Neither will the bug reports! Help in investigating them is appreciated anytime!

## Community Report
### November 2024 Monthly Art Challenge Results
For the ["Fluffy"](https://krita-artists.org/t/monthly-art-challenge-november-2024-topic-fluffy/105804) theme, 22 members submitted 26 original artworks.
And the winner is… 
[Most "Fluffy" by @steve.improvthis](https://krita-artists.org/t/monthly-art-challenge-november-2024-most-fluffy/108296), featuring three different fluffy submissions. Be sure to check out the other two as well!
<figure>
<a href="images/posts/2024/mu21_kj_s_clouds-steve_improvthis.jpeg"> <img class="fit" src="images/posts/2024/mu21_kj_s_clouds-steve_improvthis.jpeg" alt="KJ's Clouds by @steve.improvthis"/> </a>
</figure>

### The December Art Challenge is Open Now
For the December Art Challenge, @steve.improvthis has chosen ["Tropical"](https://krita-artists.org/t/monthly-art-challenge-december-2024-topic-tropical/108359) as the theme, with the optional challenge of using new or unfamiliar brushes. See the full brief for more details, and find yourself a place in the sun!

## Featured Artwork
### Best of Krita-Artists - October/November 2024
Seven images were submitted to the [Best of Krita-Artists Nominations thread](https://krita-artists.org/t/best-of-krita-artists-october-november-2024-nomination-submissions-thread/104381/), which was open from October 15th to November 11th. When the poll closed on November 14th, these five wonderful works made their way onto the Krita-Artists featured artwork banner:

[Ocean | Krita](https://krita-artists.org/t/105853) by @Gurkirat_Singh
<figure>
<a href="images/posts/2024/mu21_ocean-gurkirat_singh.jpeg"> <img class="fit" src="images/posts/2024/mu21_ocean-gurkirat_singh.jpeg" alt="Ocean by @Gukirat_Singh"/> </a>
</figure>

[Winter palace](https://krita-artists.org/t/106343) by @Sad_Tea
<figure>
<a href="images/posts/2024/mu21_winter_palace-sad_tea.jpeg"> <img class="fit" src="images/posts/2024/mu21_winter_palace-sad_tea.jpeg" alt="Winter palace by @Sad_Tea"/> </a>
</figure>

[Order](https://krita-artists.org/t/90378) by @Valery_Sazonov
<figure>
<a href="images/posts/2024/mu21_order-valery_sazonov.jpeg"> <img class="fit" src="images/posts/2024/mu21_order-valery_sazonov.jpeg" alt="Order by @Valery_Sazonov"/> </a>
</figure>

[Curly, 10-24](https://krita-artists.org/t/104018) by @Celes
<figure>
<a href="images/posts/2024/mu21_curly_10-24-celes.jpeg"> <img class="fit" src="images/posts/2024/mu21_curly_10-24-celes.jpeg" alt="Curly, 10-24 by @Celes"/> </a>
</figure>

[Afternoon Magic](https://krita-artists.org/t/106342) by @zeki
<figure>
<a href="images/posts/2024/mu21_afternoon_magic-zeki.jpeg"> <img class="fit" src="images/posts/2024/mu21_afternoon_magic-zeki.jpeg" alt="Afternoon Magic by @zeki"/> </a>
</figure>


## Ways to Help Krita
Krita is Free and Open Source Software developed by an international team of sponsored developers and volunteer contributors.

Visit [Krita's funding page](https://krita.org/en/donations/) to see how user donations keep development going, and explore a one-time or monthly contribution. Or check out more ways to [Get Involved](https://krita.org/en/get-involved/), from testing, coding, translating, and documentation writing, to just sharing your artwork made with Krita.

The Krita-promo team has put out a [call for volunteers](https://krita-artists.org/t/request-for-krita-promo-monthly-update-volunteers/101404/), come join us and help keep these monthly updates going.

## Notable Changes
Notable changes in Krita's development builds from Nov. 12 - Dec. 11, 2024.

### Stable branch (5.2.9-prealpha):
* General: Fix rounding errors in opacity conversion, which prevented layered 50% brushstrokes from adding up to 100%. ([bug report](https://bugs.kde.org/356462)) ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2268), by Dmitry Kazakov)
* General: Fix snapping to grid at the edge of the canvas. ([bug report](https://bugs.kde.org/492434)) ([Change](https://invent.kde.org/graphics/krita/-/commit/f639df17fe), by Dmitry Kazakov)
* General: Disable snapping to image center by default, as it can cause confusion. ([bug report](https://bugs.kde.org/466718)) ([Change](https://invent.kde.org/graphics/krita/-/commit/5224bfa36e), by Dmitry Kazakov)
* Calligraphy Tool: Fix following existing shape in the Calligraphy Tool. ([bug report](https://bugs.kde.org/433288)) ([Change](https://invent.kde.org/graphics/krita/-/commit/063b4d386a), by Dmitry Kazakov)
* Layers: Fix "Copy into new Layer" to copy vector data when a vector shape is active. ([bug report](https://bugs.kde.org/418317)) ([Change](https://invent.kde.org/graphics/krita/-/commit/75b20e7594), by Dmitry Kazakov)
* Selections: Fix the vector selection mode to not create 0px selections, and to select the canvas beforing subtracting if there is no existing selection. ([bug report](https://bugs.kde.org/445935), CC[bug report](https://bugs.kde.org/408369)) ([Change](https://invent.kde.org/graphics/krita/-/commit/c3279cd2ae), by Dmitry Kazakov)
* General: Add Unify Layers Color Space action. ([Change](https://invent.kde.org/graphics/krita/-/commit/e0529a5168), by Dmitry Kazakov)
* Layers: Don't allow moving a mask onto a locked layer. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2273), by Maciej Jesionowski)
* Linux: Capitalize the .AppImage file extension to match the convention expected by launchers. ([bug report](https://bugs.kde.org/447445)) ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2269), by Dmitry Kazakov)

### Unstable branch (5.3.0-prealpha):
Bug fixes:
* Color Management: Update display rendering when blackpoint compensation or LCMS optimizations are toggled, not just when the display color profile is changed. ([bug report](https://bugs.kde.org/496388)) ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2277), by Dmitry Kazakov)

Features:
* Text: Implement Convert to Shape for bitmap fonts. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2261), by Wolthera van Hövell)
* Filters: Add Fast Color Overlay filter, which overlays a solid color using a configurable blending mode. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2282), by Maciej Jesionowski)
* Brush Engines: Add Pattern option to "Auto Invert For Eraser" mode. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2264), by Dmitry Kazakov)
* Wide Gamut Color Selector Docker: Add option to hide the Minimal Shade Selector rows. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2274), by Wolthera van Hövell)
* Wide Gamut Color Selector Docker: Show the Gamut Mask toolbar when the selector layout supports it. ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2275), by Wolthera van Hövell)
* Layers: Add a warning icon for layers with a different color space than the image. ([Change 1](https://invent.kde.org/graphics/krita/-/commit/12eb4d8355), by Dmitry Kazakov, and [Change 2](https://invent.kde.org/graphics/krita/-/merge_requests/2278), by Timothée Giet)
* Pop-Up Palette: Add an option to sort the color history ring by last-used instead of by color. ([bug report](https://bugs.kde.org/441900)) ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2266), by Dmitry Kazakov)
* Export Layers Plugin: Add option to use incrementing prefix on exported layers. ([wishbug report](https://bugs.kde.org/424331)) ([Change](https://invent.kde.org/graphics/krita/-/merge_requests/2267), by Ross Rosales)


## Nightly Builds
Pre-release versions of Krita are built every day for testing new changes.

Get the latest bugfixes in **Stable** "Krita Plus" (5.2.9-prealpha): [Linux](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/windows) - [macOS (unsigned)](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/macos) - [Android arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm64-v8a) - [Android arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm32-v7a) - [Android x86_64](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-x86_64)

Or test out the latest **Experimental** features in "Krita Next" (5.3.0-prealpha). Feedback and bug reports are appreciated!: [Linux](https://cdn.kde.org/ci-builds/graphics/krita/master/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/master/windows) - [macOS (unsigned)](https://cdn.kde.org/ci-builds/graphics/krita/master/macos) - [Android arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm64-v8a) - [Android arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm32-v7a) - [Android x86_64](https://cdn.kde.org/ci-builds/graphics/krita/master/android-x86_64)

<!--
## Have feedback?
Join the [discussion of this post] () on the [Krita-Artists forum](https://krita-artists.org/)!-->
