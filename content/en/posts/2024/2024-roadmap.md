---
title: "What We're Up To In 2024"
date: "2024-03-14"
---
It's 2024 already, and even already March. Like last year, we had a video call with all sponsored developers, artists and volunteers to discuss what we achieved last year, figure out the biggest issues we're facing and set the priorities for this year.


# Challenges


A very serious issue is that the maintainer of the Android and ChromeOS port of Krita has become too busy to work on Krita full-time. The Android and ChromeOS versions of Krita both use the Android platform, and that platform changes often and arbitrarily. This means that Sharaf has spent almost all of his time keeping Krita running on Android (and ChromeOS), instead of, as we had planned, work on a dedicated tablet user interface for Krita on Android. And since that maintenance work now is not being done, we're having a really big problem there. Additionally, since KDE has retired the binary factory and moved binary builds to invent.kde.org's continuous integration system, we don't have automatic builds for Android anymore.

We've also lost another sponsored developer. They were sick for quite some time, but recently they blogged they had started to work a different job. Since they were especially working on maintaining the libraries Krita is dependent on and were very good at upstreaming fixes, they will also really be missed.

Finally, we got Krita into the Apple MacOS store last year. However, two years ago, Krita's maintainer, that's me, changed her legal name. Now the certificates needed to sign the package for the store have expired, and we needed to create new certificates. Those have to have the signer's current legal name, and for some reason, it's proving really hard get the store to allow that the same developer, with the same ID and code but a different legal name to upload packages. We're working on that.

# What We Did Last Year

Of course, we released Krita 5.2 and two bugfix releases for Krita 5.2. We'll do at least one other bugfix release before we release Krita 5.3.

The audio system for Krita's animation feature got completely overhauled, ported away from Qt's QtMultimedia system to MLT, The storyboard feature got improved a lot, we gained JPEG-XL support just in time for Google's Chrome team to decide to drop it, because there was nobody supporting it... We also refactored the system we use to build all dependent libraries on all platforms. Well, work on MacOS is still going on, with PyQt being a problem point. Of course, there were a lot of other things going on as well.

Wolthera started rewriting the text object, and mostly finished that and now is working on the tool to actually write, modify and typeset text. This is a huge change with very impressive results!

# What We Hope To Do This Year

Parts of this list is from last year, part of it is new.

One big caveat: now that the KDE project has released the first version of KDE Frameworks for Qt6, porting Krita to Qt6 is going to have to happen. This is a big project, not just because of disappearing functions, but very much because of the changes to the support for GPU rendering. On Windows, OpenGL drivers are pretty buggy, and because of that, Qt5 offered the possibility to use the Angle compatibility layer between applications that use OpenGL and the native Direct3D library for GPU rendering. That's gone, and unless we rewrite our GPU rendering system, we need to put Angle back into the stack.

All in all, it's pretty likely that porting to Qt6 will take a lot of time away from us implementing fun new features. But when that is done we can start working on a tablet-friendly user interface, provided we can still release Krita for Android.

That's not to say we don't want to implement fun new featurs!

Here's the shortlist:

* Implement a system to create flexible text balloons and integrate that with the text object so the text flows into the balloons
* Implement a new layer type, for comic book Frameworks
* Provide integration with Blender. (This is less urgent, though, since there is a very useful third-party plugin for that already: [Blender Layer](https://github.com/Yuntokon/BlenderLayer/))
* Replace the current docker system with something more flexible, and maintained.
* Implement a system to provide tool presets
* Create a new user interface for handling palettes
* Add an animation audio waveform display
* Add support for animation reference frame workflow.

We also discussed using the GPU for improving performance. One original idea was to use the GPU for brushes, but the artists argued that the brush performance is fine, and what's way too slow are the liquefy transform tool, transform masks and some filters. In the end, Dmitry decided to investigate

* optimizing transform masks on the GPU

And there's the most controversial thing of all: should we add AI features to Krita? We have had several heated discussions amongst developers and artists on the mailing list and on invent.kde.org.

The artists in the meeting argued that generative AI is worthless and would at best lead to bland, repetitive templates, but that assistive AI could be useful. In order to figure out whether that's true, we started investigating one particular project: AI-assisted inking of sketches. This is useful, could replace a tedious step when doing art while still retaining the artistic individuality. Whether it will actually make it to Krita is uncertain of course, but the investigation will hopefully help us understand better the issue, the possibilities and the problems.

Note: we won't be implementing anything that uses models trained on scraped images and we will make sure that the carbon footprint of the feature doesn't exceed its usefulness.
