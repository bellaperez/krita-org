---
title: "Krita Monthly Update – Edition 16"
date: "2024-06-21"
---
Welcome to the latest development and community news curated for you by the [Krita-promo](https://krita-artists.org/g/krita-promo) team.

## Development report

### Krita is 25 years old!

![krita_25th_davidrevoy_ccbysa](images/posts/2024/david-kiki-krita-25.jpeg)
*Artwork by [David Revoy](https://www.davidrevoy.com/article1031/happy-25th-anniversary-krita) (CC BY-SA)*

May 31, 2024 marks Krita’s 25th birthday. As one would expect, there have been many changes over the years – even the name changed several times. You can get a look inside Krita’s history in this [blog post written by @Halla](https://krita.org/en/posts/2024/krita-25-years/), Krita’s Maintainer for more than 20 years.

In honour of this milestone, @RamonM prepared a special treat for all Krita users: a video interview with @Halla.

 {{< youtube iHPUoyeWs3I >}}

 ### Your feedback is requested

 * 5.2.3-Beta1 was released June 5th. This release represents a complete rework of the build system and numerous fixes by the core Krita developer team as well as [freyalupen](https://krita-artists.org/u/freyalupen), [Grum999](https://krita-artists.org/u/grum999), [NabilMaghfurUsman](https://krita-artists.org/u/nabilmaghfurusman), [Deif_Lou](https://krita-artists.org/u/deif_lou), Alvin Wong, Rasyuqa A. H. and Mathias Wein. There are a number of first-time contributors whose names appear next to their contribution in the [release notes](https://krita.org/en/posts/2024/krita-5-2-3-beta1/).

* Testing packages for every platform are provided on the release notes page. Please report your findings and feedback in the [Testers Wanted](https://krita-artists.org/t/testers-wanted-5-2-3-beta1-issues-and-feedback-thread/93122) thread.


* Text property editing: Merge request [2092](https://invent.kde.org/graphics/krita/-/merge_requests/2092) is almost finished, awaiting review. Testing builds are now available on the CI. @Wolthera is requesting user feedback on the UX. Please read [this post](https://krita-artists.org/t/text-tool-thread/57973/102) and share your comments there.
* Google Summer of Code (GSOC) @Ken_Lo is seeking input on the Pixel Perfect project https://krita-artists.org/t/pixel-perfect-line-setting-for-pixel-art-brushes/42629/16

### Other Development Highlights

* [Free transform bounding box rotation](https://invent.kde.org/graphics/krita/-/merge_requests/2113) by [Stuffin](https://krita-artists.org/u/stuffin) has been merged. This completes the feature request [Adjusting the transform box to match the object angle in the drawing](https://krita-artists.org/t/adjusting-the-transform-box-to-match-the-object-angle-in-the-drawing/63687) and can be tested in the 5.3.0-prealpha nightly. (Note to testers: Adjusting the bounding box is activated with Ctrl+Alt.) Thanks stuffin!

{{< video-player src="videos/posts/2024/boundingbox_stuffinUPDATED.mp4" type="video/mp4" >}}


* Grum999 is improving the python API so that it is more robust for python developers and they can access more of krita’s internal features through python. There is a work-in-progress MR to add new scripting functions for accessing Grids, Guides, and Mirror Axes from the document, and signals for changes in the document and view. [Check the MR](https://invent.kde.org/graphics/krita/-/merge_requests/2147)

* @Ralek has added lossless transformation conditions - Rotations in increments of 90 degrees, and perfect x and y mirrors should now be lossless. This should greatly help out pixel artists, who I believe previously could not use these functions at all. [Check the MR](https://invent.kde.org/graphics/krita/-/merge_requests/2137)

![transform](images/posts/2024/transform-ralek.png)

## Community Report

### May 2024 Monthly Art Challenge

And the winner is… [Cat Reflection by Elixiah](https://krita-artists.org/t/reflection-may-2024-challenge-winner/92633).

![cat reflection by Elixiah](images/posts/2024/elixiah-moa.jpeg)


For the June Art Challenge, Elixiah has chosen [Magnificent Dragon](https://krita-artists.org/t/monthly-art-challenge-june-2024-topic-magnificent-dragon/92747/17) with an interesting optional challenge for any who care to give themselves an added stretch.


### Featured artwork

Ten images were submitted to the [Best of Krita-Artists Nominations thread](https://krita-artists.org/t/best-of-krita-artists-april-may-nomination-submissions-thread/89210) which was open from April 14th to May 11th. When voting closed on May 14th, these five had the most votes and were added to the Krita-Artists featured artwork banner.


[Quiet Morning](https://krita-artists.org/t/quiet-morning-painted-in-krita/78326) by @Gurkirat_Singh.
![Quiet-morning](images/posts/2024/quiet-morning.jpeg)


[Pollinatrix Terrae](https://krita-artists.org/t/pollinatrix-terrae/89278) by @jimplex.

![Pollinatrix](images/posts/2024/pollinatrix-terrae.jpeg)

[The Lone Rider-2](https://krita-artists.org/t/the-lone-rider-2/88354) by @rohithela.

![](images/posts/2024/lone-rider-2.jpeg)

[005 (Spider in the web)](https://krita-artists.org/t/005-spider-in-the-web/89809) by @HappyBuket.

![005 Spider](images/posts/2024/005-spider-in-the-web.jpeg)

[Challenge Horn](https://krita-artists.org/t/challenge-horn/88213) by @MangooSalade.

![Challenge Horn](images/posts/2024/challenge-horn.jpeg)


In addition to their place of honour on the banner, all five will be entered into the Best of Krita-Artists 2024 competition next January. The [Best of Krita-Artists May/June Nominations thread](https://krita-artists.org/t/best-of-krita-artists-may-june-2024-nomination-submissions-thread/91535) will be open for submissions until June 11, 2024. You are invited to join in by nominating your favourite piece of Krita artwork!


## Noteworthy Plugin

[Create a New View as Window and Topped](https://krita-artists.org/t/create-a-new-view-as-windowed-and-topped/59900) by Cliscylla saves steps by opening a new view and setting it to always stay on top.
![new window plugin](images/posts/2024/new-view-window.png)

## Tutorial of the month
**How to record video directly from Krita and post to social media** by Deevad is a comprehensive tutorial for beginner and intermediate Krita users. It takes the viewer through the initial screen set up and recommended canvas dimensions right through to the export process.

{{< youtube 9DpLL2ee9LU >}}

## Ways to help Krita

Krita is a Free and Open Source application, mostly developed by an international team of enthusiastic volunteers. Donations from Krita users to support maintenance and development is appreciated.

Visit [Krita’s funding page](https://krita.org/en/donations/) to see how donations are used and explore a one-time or monthly contribution.


## Notable Changes in the code

This section has been compiled by freyalupen.
(May 6 - June 6, 2024)

---

### Stable branch (5.2.3-beta1):
**Bugfixes:**

* **General** Don't waste memory generating empty animation frames on images with no animation. This was a regression in 5.2.x. ([commit, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/commit/35239d9b9e2149dde816af077ede5fa27d6d7484))
* **Storyboard Docker** Fix reordering storyboard scenes causing all frame data to be deleted while still appearing to be present. ([BUG:476440](https://bugs.kde.org/476440)) ([merge request, Freya Lupen](https://invent.kde.org/graphics/krita/-/merge_requests/2141))
* **Android: Animation** Fix crash when attempting to load audio on Android, a regression present in 5.2.2.1. ([merge request, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/merge_requests/2153))

**Stable branch (5.2.3-beta1+):**

Bugfixes:

* **Animation** Fix crash when adding a keyframe column with a locked layer selected. ([BUG:486893](https://bugs.kde.org/486893)) ([commit, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/commit/7541cfa30c29abc0c1050c6d3c3dab496995f572))
* **Keyboard shortcuts** While continuing making a Selection, ignore other modifier shortcuts to avoid conflicts. ([merge request, Aqaao](https://invent.kde.org/graphics/krita/-/merge_requests/2138))
* **File Formats: TIFF** Ask to use PSD data in TIFF only if any was found. ([BUG:488024](https://bugs.kde.org/488024)) ([commit, Freya Lupen](https://invent.kde.org/graphics/krita/-/commit/8901d5460d897ee7c2311038642e082b13aa6b47))
* **General, macOS** Fix update of "read-only" state of the document when loading and saving. Fixes a crash on macOS when loading TIFF or JPEG-XL recent file icons (which load a temporary document). ([BUG:487544](https://bugs.kde.org/487544)) ([commit, Dmitry Kazakov](https://invent.kde.org/graphics/krita/-/commit/4e882db22bb0064b4faa353125928728bad66567))
* **Android, Recorder Docker** Fix saving Recorder frames as JPEG on Android, a regression present in 5.2.2.1. ([BUG:487667](https://bugs.kde.org/487667)) ([commit, Dmitry Kazakov](https://invent.kde.org/szaman/qtbase/-/commit/c84b3651b33e168998503c547698c60d67553a2b))
* **Android, General** Improve Krita's apk icons to follow Android design guidelines. ([BUG:463043](https://bugs.kde.org/463043)) ([merge request, Jesse 205](https://invent.kde.org/graphics/krita/-/merge_requests/2150))

### Features:

* **Scripting** Generate a Python type stub file for Krita's API, which can be used to setup type auto-completion in IDEs, located inside the Krita package at /lib/krita-python-libs/PyKrita/krita.pyi. ([merge request, Kate Corcoran](https://invent.kde.org/graphics/krita/-/merge_requests/2156))

Stable branch (5.2.3-beta1+) backports from Unstable:
Bugfixes:

* **Recorder Docker** Reworked default recorder docker FFmpeg profiles. If canvas size changes during recording, the export profiles now keep aspect instead of stretching ([BUG:429326](https://bugs.kde.org/429326)). Issues with resize, result preview, and extend result are avoided ([BUG:455006](https://bugs.kde.org/455006), [BUG:450790](https://bugs.kde.org/450790), [BUG:485515](https://bugs.kde.org/485515), [BUG:485514](https://bugs.kde.org/485514)). For MP4, detect whether openh264 or libx264 is present instead of using separate profiles. Also, prevent an error when using FFmpeg 7. ([merge request, Ralek Kolemios](https://invent.kde.org/graphics/krita/-/merge_requests/2124))
* **Selection Tools**  Fix issue making selections on color-labeled reference selections. ([BUG:486419](https://bugs.kde.org/486419)) ([commit, Deif Lou](https://invent.kde.org/graphics/krita/-/commit/66dd431ca6d548ca7e169cc7c66568df3243f417))

---

### Unstable branch (5.3.0-prealpha):

**Features:**

* **Transform Tool** Allow rotating the free transform bounding box with Ctrl+Alt, in order to make transformations along an arbitrary axis. ([WISHBUG:383587](https://bugs.kde.org/383587)) ([merge request, Stuffins](https://invent.kde.org/graphics/krita/-/merge_requests/2113))

Bugfixes:

* **Transform Tool** Make sure perfect mirrors and 90-degree rotations are transformed losslessly. ([merge request, Ralek Kolemios](https://invent.kde.org/graphics/krita/-/merge_requests/2137))
* **Shortcuts** Fix Sample Screen Color getting stuck if activated multiple times without completing. ([BUG:485739](https://bugs.kde.org/485739)) ([merge request, Deif Lou](https://invent.kde.org/graphics/krita/-/merge_requests/2140))
* **Scripting** Fix setting Color Adjustment (perchannel) and Cross-Channel filters from Python scripts. ([merge request, Deif Lou](https://invent.kde.org/graphics/krita/-/merge_requests/2134))

---

These changes are made available for testing in the following Nightly builds:

* **Stable "Krita Plus" (5.2.3-beta1+)**: [Linux](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/windows) - [macOS](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/macos) [unsigned currently] - Android ([arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm64-v8a) / [arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-arm32-v7a) / [x86_64](https://cdn.kde.org/ci-builds/graphics/krita/krita-5.2/android-x86_64))
* **Unstable "Krita Next" (5.3.0-prealpha)**: [Linux](https://cdn.kde.org/ci-builds/graphics/krita/master/linux) - [Windows](https://cdn.kde.org/ci-builds/graphics/krita/master/windows) - [macOS](https://cdn.kde.org/ci-builds/graphics/krita/master/macos) [unsigned currently] - Android ([arm64-v8a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm64-v8a) / [arm32-v7a](https://cdn.kde.org/ci-builds/graphics/krita/master/android-arm32-v7a) / [x86_64](https://cdn.kde.org/ci-builds/graphics/krita/master/android-x86_64))

{{< support-krita-callout >}}
