---
title: "New: the Fast Sketch Plugin for Krita"
date: "2024-12-19"
categories: 
  - "news"
  - "officialrelease"
---

Together with [Intel](https://intel.com/), we have been working a new plugin for Krita: the fast sketch plugin, or maybe, better, a fast inking plugin. This is an experimental plugin that makes it (sometimes) possible to automatically ink a sketch, using neural networks.

This plugin uses models to figure out how to ink a sketch: the included models were trained on openly available data: there was no scraping or stealing involved! The plugin comes with a manual that explains how to get the scripts you can use to create a model trained on your own data: what you need are before and after images of your sketch and your uncolored inked drawing, and the training software can run on your own hardware (it will take a lot of time, though).

Throughout the development process we've been discussing this plugin with artists on the [Krita Artists forum](https://krita-artists.org/t/introducing-a-new-project-fast-line-art/94265).

![Three iterations of running the fast sketch plugin on a sketch](https://krita.org/images/posts/2024/fast_sketch_plugin.jpg)
*Artwork by [@BeARToys](https://krita-artists.org/t/fast-sketch-cleanup-plugin-first-public-testing/109066/8) (CC BY-SA)*


The plugin can be downloaded and extracted in a Windows Krita 5.2.6 folder and should then be enabled in the plugin manager in Krita's settings dialog.

There is also a download of Krita 5.3.0 pre-alpha available that includes the plugin for Windows and Linux. Currently, we don't have a working MacOS version ready, and since the plugin is implemented in Python, there will be no Android packages.

## Download

### Windows

- Plugin: Fast Sketch Plugin 1.0.2 [FastSketchPlugin1.0.2.zip](https://download.kde.org/stable/krita/FastSketchPlugin-1.0.2/FastSketchPlugin1.0.2.zip)
- Portable zip file: Krita 5.3.0 pre-alpha [krita-x64-5.3.0-prealpha-cdac9c31.zip](https://download.kde.org/unstable/krita/5.3.0-prealpha-fast-sketch/krita-x64-5.3.0-prealpha-cdac9c31.zip)

### Linux

- 64 bits Linux: [krita-x64-5.3.0-prealpha-cdac9c31.zip](https://download.kde.org/unstable/krita/5.3.0-prealpha-fast-sketch/krita-5.3.0-prealpha-cdac9c31c9-x86_64.AppImage)

