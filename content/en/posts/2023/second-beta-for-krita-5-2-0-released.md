---
title: "Second Beta for Krita 5.2.0 Released"
date: "2023-08-17"
categories: 
  - "development-builds"
---

After two weeks of bug fixing, it is already time for the second 5.2 beta! In addition to [the changes that 5.2 brings](en/release-notes/krita-5-2-release-notes), we have fixed the following bugs in this second beta:

- Recent files are now front and center, news items have more text.
- Make new and open file buttons a bit bigger.
- Update banner
- Disable selection in recent files list on welcome page
- Fix welcome page layout flicker after closing the last file
- Greatly improve the situation surrounding bitmap glyphs and general glyph retrieval ([Bug 472566](https://bugs.kde.org/show_bug.cgi?id=472566))
- Add synthesizing italic and bold. We cannot disable these right now, but that was also the case with the previous text layout, and synthesizing italics and bolds is very important for East-Asian fonts. ([Bug 465886](https://bugs.kde.org/show_bug.cgi?id=465886))
- svgtexttool: Disable Shift+Enter soft line breaks ([Bug 472743](https://bugs.kde.org/show_bug.cgi?id=472743))
- Fix text-decoration not getting cleared when set as ‘none’. ([Bug 466130](https://bugs.kde.org/show_bug.cgi?id=466130))
- Fix line-height being capped due to naive handling of negative ascent.([Bug 472316](https://bugs.kde.org/show_bug.cgi?id=472316))
- svgtexttool: Allow Esc to cancel text shape creation when dragging
- Fix rendering of text-decoration thickness ([Bug 466130](https://bugs.kde.org/show_bug.cgi?id=466130))
- Let baseline adjustments be taken into account for line-height calculation.
- Fix incorrect baseline value by reinitializing the variable to 0 ([Bug 472464](https://bugs.kde.org/show_bug.cgi?id=472464)).
- Subtract the origin from baseline value to get appropriate baseline offset ([Bug 472464](https://bugs.kde.org/show_bug.cgi?id=472464))
- Don’t strip any whitespaces from SVG text/tspan nodes
- Fix SVG text-transform param ‘none’
- text: Fix bounding box of bitmap glyphs in vertical mode ([Bug 472566](https://bugs.kde.org/show_bug.cgi?id=472566))
- Embed our font libraries into the AppImage ([Bug 472710](https://bugs.kde.org/show_bug.cgi?id=472710))
- Fix fill not painting because it was not using the boundingRect ([Bug 473026](https://bugs.kde.org/show_bug.cgi?id=473026)).
- Fix initialization of the fontconfig embedded into AppImage ([Bug 473161](https://bugs.kde.org/show_bug.cgi?id=473161))
- Stabilize the outline code a little ([Bug 473164](https://bugs.kde.org/show_bug.cgi?id=473164)).
- Enable text tool to draw debug outlines of text shapes in view. This debug function can be enabled by setting the environment variable `KRITA_DEBUG_TEXTTOOL`.
- Attempt to position the first few glyphs inside a shape ([Bug 472571](https://bugs.kde.org/show_bug.cgi?id=472571))
- Make transformation resolution happen earlier and be a bit better aligned to the SVG 2 spec ([Bug 472382](https://bugs.kde.org/show_bug.cgi?id=472382))
- Handling the hanging of white-spaces, including partial and conditional hang ([Bug 472829](https://bugs.kde.org/show_bug.cgi?id=472829)).
- Improve justification of text.
- Implement arbitrary run breaks inside raqm to ensure that absolute chunks are broken up ([Bug 471374](https://bugs.kde.org/show_bug.cgi?id=471374)).
- text: Skip Unicode Format (Cf) characters in font matching
- text: Check UTF-32 when deciding to skip font matching
- Revert “Ensure pen brush (opposed to eraser brush) is selected on new document” ([Bug 472730](https://bugs.kde.org/show_bug.cgi?id=472730))
- Snap to exact 15 degree increments when using discrete rotate ([Bug 472839](https://bugs.kde.org/show_bug.cgi?id=472839))
- Fix angle offset in Drawing Angle sensor ([Bug 473103](https://bugs.kde.org/show_bug.cgi?id=473103))
- Fix masking brush size issues (again) ([Bug 469604](https://bugs.kde.org/show_bug.cgi?id=469604))
- Fix sharpness option being uninitialized ([Bug 472779](https://bugs.kde.org/show_bug.cgi?id=472779), [Bug 472723](https://bugs.kde.org/show_bug.cgi?id=472723))
- Huge amount of work done by Dmitry to make the new MLT engine handle better, thanks to this all sorts of small bugs were fixed: Including the loss of the 'drop frames during playback' functionality and issues with large animations.
- Fix warning on loading animation xml
- Fix a signal-slot connection
- logdocker: Don’t interfere with default logging to console
- Show main window when closing session manager ([Bug 431755](https://bugs.kde.org/show_bug.cgi?id=431755))
- Translation: Disambiguated the string “Static” and “Dynamic” for Wide Gamut Color Selector.

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the Krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-x64-5.2.0-beta2-setup.exe](https://download.kde.org/unstable/krita/5.2.0-beta2/krita-x64-5.2.0-beta2-setup.exe)
- Portable 64 bits Windows: [krita-x64-5.2.0-beta2.zip](https://download.kde.org/unstable/krita/5.2.0-beta2/krita-x64-5.2.0-beta2.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/unstable/krita/5.2.0-beta2/krita-x64-5.2.0-beta2-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.2.0-beta2-x86\_64.appimage](https://download.kde.org/unstable/krita/5.2.0-beta2/krita-5.2.0-beta2-x86_64.appimage)

The separate gmic-qt AppImage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.2.0-beta2.dmg](https://download.kde.org/unstable/krita/5.2.0-beta2/krita-5.2.0-beta2.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface requires a large screen. _The Android release is not signed for beta 2_.

- [64 bits Intel CPU APK](https://download.kde.org/unstable/krita/5.2.0-beta2/krita-x86_64-5.2.0-beta2-release-unsigned.apk)
- [32 bits Intel CPU APK](https://download.kde.org/unstable/krita/5.2.0-beta2/krita-x86-5.2.0-beta2-release-unsigned.apk)
- [64 bits Arm CPU APK](https://download.kde.org/unstable/krita/5.2.0-beta2/krita-arm64-v8a-5.2.0-beta2-release-unsigned.apk)
- [32 bits Arm CPU APK](https://download.kde.org/unstable/krita/5.2.0-beta2/krita-armeabi-v7a-5.2.0-beta2-release-unsigned.apk)

### Source code

- [krita-5.2.0-beta2.tar.gz](https://download.kde.org/unstable/krita/5.2.0-beta2/krita-5.2.0-beta2.tar.gz)
- [krita-5.2.0-beta2.tar.xz](https://download.kde.org/unstable/krita/5.2.0-beta2/krita-5.2.0-beta2.tar.xz)

### md5sum

For all downloads, visit [https://download.kde.org/unstable/krita/5.2.0-beta2/](https://download.kde.org/unstable/krita/5.2.0-beta2) and click on Details to get the hashes.

### Key

The Linux AppImage and the source .tar.gz and .tar.xz tarballs are signed. This particular release is signed with a non-standard key, you can retrieve is [here](https://files.kde.org/krita/4DA79EDA231C852B) or download from the public server:

gpg --recv-keys E9FB29E74ADEACC5E3035B8AB69EB4CF7468332F

The signatures are [here](https://download.kde.org/unstable/krita/5.2.0-beta2/) (filenames ending in .sig).
