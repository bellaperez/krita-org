import os
import sys
import fileinput

###############
## Swaps out one-time payment API Key stored at server
###############

if os.environ.get("CI_COMMIT_REF_NAME") == "master":
    textToSearch = "MOLLIEAPIKEY"

    # replace with environment variable/secret later
    textToReplace = os.environ.get('MOLLIE_API_KEY', 'MOLLIEAPIKEY')

    # File to perform Search-Replace on
    fileToSearch  = 'static/php/mollie-one-time-donation.php'

    # go through each line and swap out text if found
    for line in fileinput.input( fileToSearch, inplace=True ):
        if textToSearch in line :
            line = line.replace(textToSearch, textToReplace)
        sys.stdout.write(line)

###############
## Compile translations from PO files and update translations
###############
os.environ["PACKAGE"] = 'websites-krita-org'
os.system('git clone https://invent.kde.org/websites/hugo-i18n && pip3 install ./hugo-i18n')
os.system('hugoi18n compile po')  # compile translations in folder "po"
os.system('hugoi18n generate')
